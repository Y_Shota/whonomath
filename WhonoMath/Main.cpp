
//#include "Math/Math.hpp"
#include "Math/Scalar/Constant.hpp"
#include "Math/Scalar/Trigonometric.hpp"

using namespace Whono;
using namespace Math;


int main() {

	// VectorT<Float, 3> vec{ 1, 2, 3 };
	// auto* p = &vec;
	// auto isZero = vec.IsZero();
	// auto len = vec.Length();
	// auto cross = vec.Cross(vec);
	// auto x = vec.X();
	// auto y = vec.Y();
	// auto z = vec.Z();
	//
	// auto s = sizeof(VectorT<char, 4>);
	//
	// VectorT<int, 2> vec1{4, 5};
	//
	// auto equal = vec == vec1;
	//
	// auto vec2 = vec1 + vec;
	//
	// auto betweenAngle = BetweenAngle(vec1, vec2);
	//
	// MatrixImpl<float, 4, 4> mat{
	// 	{1,0,0,0},
	// 	{0,1,0,0},
	// 	{0,0,1,0},
	// 	{0,0,0,1},
	// };
	// auto adj = mat.Adjugate();
	// auto det = mat.Determinant();
	// mat += 1;
	//
	// auto vec3 = Vector2Of(1, 2);
	// auto vec4 = VectorFill3dOf(4);
	//
	// auto deg = DegreeOf(180);
	// auto rad = deg.Radian();
	//
	// auto trans = Matrix2dTranslation({ 1, 2 });

	auto theta = PI_DIV2;
	auto s = Sin(theta);
	auto c = Cos(theta);
	
	int a = 0;
}