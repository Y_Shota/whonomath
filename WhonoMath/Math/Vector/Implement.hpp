#pragma once
#include <array>

#include "CrossProduct.hpp"
#include "../TypeTraits.hpp"
#include "../Scalar.hpp"

namespace Whono::Math {


template<Scalar _T, Size _Dimension>
class VectorImpl : public CrossProduct<_T, _Dimension> {

public:
	using ValueType = _T;
	using RValueType = _T&;
	using CRValueType = const _T&;
	using LValueType = _T&&;
	using PValueType = _T*;
	using CPValueType = const _T*;
	using FloatingValueType = ToFloatingPointType<_T>;

	static constexpr auto DIMENSION = _Dimension;
	
private:
	union {
		ValueType rawData_[DIMENSION];
		std::array<ValueType, DIMENSION> data_;
	};

public:
	[[nodiscard]] constexpr VectorImpl()
		: data_{ 0 } {}
	
	[[nodiscard]] explicit constexpr VectorImpl(CRValueType s)
		: VectorImpl() {

		data_.fill(s);
	}

	template<Scalar... _Args>
	requires (DIMENSION == sizeof...(_Args))
	[[nodiscard]] constexpr VectorImpl(_Args... args)
		: data_{ static_cast<ValueType>(args)... } {}

	template<Scalar _U, Size _Size>
	requires (DIMENSION >= _Size)
	[[nodiscard]] explicit constexpr VectorImpl(const std::array<_U, _Size>& rhv)
		: VectorImpl() {

		for (auto i = 0; i < _Size; ++i)	data_[i] = rhv[i];
	}
	
	template<Scalar _U, Size _Size, Size _ComNum = GetMinSize<_Size, DIMENSION>>
	[[nodiscard]] constexpr VectorImpl(const VectorImpl<_U, _Size>& rhv)
		: VectorImpl() {
		
		for (auto i = 0; i < _ComNum; ++i)	data_[i] = static_cast<ValueType>(rhv[i]);
	}
	template<Scalar _U, Size _Size, Size _ComNum = GetMinSize<_Size, DIMENSION>>
	auto operator=(const VectorImpl<_U, _Size>& rhv) -> VectorImpl& {

		Clear();
		for (auto i = 0; i < _ComNum; ++i)	data_[i] = static_cast<ValueType>(rhv[i]);
		return *this;
	}
	
	[[nodiscard]] constexpr VectorImpl(const VectorImpl&) = default;
	auto operator=(const VectorImpl&) -> VectorImpl& = default;
	[[nodiscard]] constexpr VectorImpl(VectorImpl&&) = default;
	auto operator=(VectorImpl&&) -> VectorImpl& = default;
	
	[[nodiscard]] auto Data() -> PValueType {

		return data_.data();
	}
	[[nodiscard]] constexpr auto Data() const -> CPValueType {

		return data_.data();
	}
	
	[[nodiscard]] auto At(Size i) & noexcept(false) -> RValueType {

		return data_.at(i);
	}
	[[nodiscard]] constexpr auto At(Size i) const& noexcept(false) -> CRValueType {

		return data_.at(i);
	}
	[[nodiscard]] constexpr auto At(Size i) const&& noexcept(false) -> ValueType {

		return std::move(data_[i]);
	}
	[[nodiscard]] auto operator[](Size i) & noexcept(false) -> RValueType {

		return data_[i];
	}
	[[nodiscard]] constexpr auto operator[](Size i) const& noexcept(false) -> CRValueType {

		return data_[i];
	}
	[[nodiscard]] constexpr auto operator[](Size i) const&& noexcept(false) -> ValueType {

		return std::move(data_[i]);
	} 

	[[nodiscard]] constexpr auto Norm() const -> FloatingValueType {

		auto buf = static_cast<FloatingValueType>(0);
		for (const auto& elem : data_)	buf += Pow2(static_cast<FloatingValueType>(elem));

		return Sqrt(buf);
	}
	[[nodiscard]] constexpr auto NormInf() const -> ValueType {

		return std::max(data_.begin(), data_.end(), [](auto&& a, auto&& b) { return Abs(a) < Abs(b); });
	}
	template<Size _Pow>
	[[deprecated("not implemented")]][[nodiscard]] constexpr auto Norm() const -> FloatingValueType {

		//累乗根が必要
		//累乗の少数指定が必要
		//指数関数、対数関数が必要
		return std::numeric_limits<FloatingValueType>::quiet_NaN();
	}
	[[nodiscard]] constexpr auto Length() const -> FloatingValueType {

		return Norm();
	}
	[[nodiscard]] constexpr auto Magnitude() const -> FloatingValueType {

		return Norm();
	}
	[[nodiscard]] constexpr auto LengthSquare() const -> FloatingValueType {

		auto buf = static_cast<FloatingValueType>(0);
		for (const auto& elem : data_)	buf += Pow2(static_cast<FloatingValueType>(elem));

		return buf;
	}
	[[nodiscard]] constexpr auto MagnitudeSquare() const -> FloatingValueType {

		return LengthSquare();
	}
	[[nodiscard]] constexpr auto IsZero() const -> Bool {

		for (const auto& elem : data_) {

			if (elem != 0)	return false;
		}
		return true;
	}
	[[nodiscard]] constexpr auto IsNormalized() const -> Bool {

		return Abs(Length() - 1) <= std::numeric_limits<FloatingValueType>::epsilon();
	}
	[[nodiscard]] constexpr auto Normalize() const -> VectorImpl<FloatingValueType, DIMENSION> {

		return VectorImpl<FloatingValueType, DIMENSION>(*this) / Length();
	}
	template<Scalar _TypeR, Size _DimR, Size _ComNum = GetMinSize<_DimR, DIMENSION>, class _RetType = GetPrecisionType<ValueType, _TypeR>>
	[[nodiscard]] constexpr auto Dot(const VectorImpl<_TypeR, _DimR>& rhv) const -> _RetType {

		auto dot = _RetType{ 0 };
		
		for (auto i = 0; i < _ComNum; ++i)	dot += data_.at(i) * rhv.At(i);

		return dot;
	}
	template<Scalar _TypeR, Size _DimR>
	[[nodiscard]] constexpr auto Distance(const VectorImpl<_TypeR, _DimR>& rhv) -> ToFloatingPointType<GetPrecisionType<ValueType, _TypeR>> {

		return (*this - rhv).Length();
	}
	
	auto Clear() & -> VectorImpl& {

		data_.fill(0);
		return *this;
	}
	auto Clear() && -> VectorImpl {

		data_.fill(0);
		return std::move(*this);
	}
	auto Fill(ValueType s) & -> VectorImpl& {

		data_.fill(s);
		return *this;
	}
	auto Fill(ValueType s) && -> VectorImpl {

		data_.fill(s);
		return std::move(*this);
	}
	template<Scalar... _Args>
	requires (DIMENSION == sizeof...(_Args))
	auto Assign(_Args... args) & -> VectorImpl& {

		data_ = { static_cast<ValueType>(args)... };
		return *this;
	}
	template<Scalar... _Args>
	requires (DIMENSION == sizeof...(_Args))
		auto Assign(_Args... args) && -> VectorImpl {

		data_ = { static_cast<ValueType>(args)... };
		return std::move(*this);
	}
	auto Set(Size i, ValueType value) & -> VectorImpl& {

		data_[i] = value;
		return *this;
	}
	auto Set(Size i, ValueType value) && -> VectorImpl {

		data_[i] = value;
		return std::move(*this);
	}

	[[nodiscard]] constexpr auto operator+() const -> VectorImpl {
		
		return VectorImpl(*this);
	}
	[[nodiscard]] constexpr auto operator-() const -> VectorImpl {

		std::array<ValueType, DIMENSION> buf{ 0 };
		for (auto i = 0; i < DIMENSION; ++i)	buf[i] = -data_[i];
		
		return VectorImpl(buf);
	}
	
	auto operator+=(ValueType rhv) -> VectorImpl& {

		for (auto& elem : data_)	elem += rhv;
		return *this;
	}
	auto operator-=(ValueType rhv) -> VectorImpl& {

		for (auto& elem : data_)	elem -= rhv;
		return *this;
	}
	auto operator*=(ValueType rhv) -> VectorImpl& {

		for (auto& elem : data_)	elem *= rhv;
		return *this;
	}
	auto operator/=(ValueType rhv) -> VectorImpl& {

		for (auto& elem : data_)	elem /= rhv;
		return *this;
	}
	
	template<Scalar _TypeR, Size _DimR>
	requires (_DimR <= DIMENSION)
	auto operator+=(const VectorImpl<_TypeR, _DimR>& rhv) -> VectorImpl& {

		for (auto i = 0; i < _DimR; ++i)	data_[i] += static_cast<ValueType>(rhv.At(i));
		return *this;
	}
	template<Scalar _TypeR, Size _DimR>
	requires (_DimR <= DIMENSION)
	auto operator-=(const VectorImpl<_TypeR, _DimR>& rhv) -> VectorImpl& {

		for (auto i = 0; i < _DimR; ++i)	data_[i] -= static_cast<ValueType>(rhv.At(i));
		return *this;
	}
	template<Scalar _TypeR, Size _DimR>
	requires (_DimR <= DIMENSION)
	auto operator*=(const VectorImpl<_TypeR, _DimR>& rhv) -> VectorImpl& {

		for (auto i = 0; i < _DimR; ++i)	data_[i] *= static_cast<ValueType>(rhv.At(i));
		return *this;
	}
	template<Scalar _TypeR, Size _DimR>
	requires (_DimR <= DIMENSION)
	auto operator/=(const VectorImpl<_TypeR, _DimR>& rhv) -> VectorImpl& {

		for (auto i = 0; i < _DimR; ++i)	data_[i] /= static_cast<ValueType>(rhv.At(i));
		return *this;
	}
};

}
