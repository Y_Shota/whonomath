#pragma once
#include "../BaseType.hpp"
#include "../TypeTraits.hpp"


namespace Whono::Math {

template<Scalar _T, Size _Dimension> class VectorImpl;

template<Scalar _T, Size _Dimension>
using VectorT = VectorImpl<_T, _Dimension>;

template<Size _Dimension>
using VectorI = VectorT<Int, _Dimension>;
template<Size _Dimension>
using VectorL = VectorT<Long, _Dimension>;
template<Size _Dimension>
using VectorF = VectorT<Float, _Dimension>;
template<Size _Dimension>
using VectorD = VectorT<Double, _Dimension>;
template<Size _Dimension>
using Vector = VectorT<BaseFloatingPoint, _Dimension>;

template<Scalar _T>
using Vector2T = VectorImpl<_T, 2>;
using Vector2I = Vector2T<Int>;
using Vector2L = Vector2T<Long>;
using Vector2F = Vector2T<Float>;
using Vector2D = Vector2T<Double>;
using Vector2 = Vector2T<BaseFloatingPoint>;

template<Scalar _T>
using Vector3T = VectorImpl<_T, 3>;
using Vector3I = Vector3T<Int>;
using Vector3L = Vector3T<Long>;
using Vector3F = Vector3T<Float>;
using Vector3D = Vector3T<Double>;
using Vector3 = Vector3T<BaseFloatingPoint>;

template<Scalar T>
using Vector4T = VectorImpl<T, 4>;
using Vector4I = Vector4T<Int>;
using Vector4L = Vector4T<Long>;
using Vector4F = Vector4T<Float>;
using Vector4D = Vector4T<Double>;
using Vector4 = Vector4T<BaseFloatingPoint>;

}