#pragma once
#include "../Angle/Implement.hpp"
#include "Implement.hpp"

namespace Whono::Math {


//生成用関数
template<Scalar _Type = BaseFloatingPoint, Scalar... _Args, Size _Dimension = sizeof...(_Args)>
[[nodiscard]] constexpr auto VectorOf(_Args... args) -> VectorImpl<_Type, _Dimension> {

	return { args... };
}
[[nodiscard]] constexpr auto Vector2Of(BaseFloatingPoint x, BaseFloatingPoint y) -> VectorImpl<BaseFloatingPoint, 2> {

	return { x, y };
}
[[nodiscard]] constexpr auto Vector3Of(BaseFloatingPoint x, BaseFloatingPoint y, BaseFloatingPoint z) -> VectorImpl<BaseFloatingPoint, 3> {

	return { x, y, z };
}
[[nodiscard]] constexpr auto Vector4Of(BaseFloatingPoint x, BaseFloatingPoint y, BaseFloatingPoint z, BaseFloatingPoint w) -> VectorImpl<BaseFloatingPoint, 4> {

	return { x, y, z, w };
}
template<Scalar _Type>
[[nodiscard]] constexpr auto VectorType2Of(_Type x, _Type y) -> VectorImpl<_Type, 2> {

	return { x, y };
}
template<Scalar _Type>
[[nodiscard]] constexpr auto VectorType3Of(_Type x, _Type y, _Type z) -> VectorImpl<_Type, 3> {

	return { x, y, z };
}
template<Scalar _Type>
[[nodiscard]] constexpr auto VectorType4Of(_Type x, _Type y, _Type z, _Type w) -> VectorImpl<_Type, 4> {

	return { x, y, z, w };
}
template<Size _Dimension>
[[nodiscard]] constexpr auto VectorFillOf(BaseFloatingPoint s) -> VectorImpl<BaseFloatingPoint, _Dimension> {

	return VectorImpl<BaseFloatingPoint, _Dimension>(s);
}
template<Size _Dimension, Scalar _Type>
[[nodiscard]] constexpr auto VectorFillTypeOf(_Type s) -> VectorImpl<_Type, _Dimension> {

	return VectorImpl<_Type, _Dimension>(s);
}
[[nodiscard]] constexpr auto VectorFill2dOf(BaseFloatingPoint s) -> VectorImpl<BaseFloatingPoint, 2> {

	return VectorImpl<BaseFloatingPoint, 2>(s);
}
[[nodiscard]] constexpr auto VectorFill3dOf(BaseFloatingPoint s) -> VectorImpl<BaseFloatingPoint, 3> {

	return VectorImpl<BaseFloatingPoint, 3>(s);
}
[[nodiscard]] constexpr auto VectorFill4dOf(BaseFloatingPoint s) -> VectorImpl<BaseFloatingPoint, 4> {

	return VectorImpl<BaseFloatingPoint, 4>(s);
}
template<Scalar _Type>
[[nodiscard]] constexpr auto VectorFillType2dOf(_Type s) -> VectorImpl<_Type, 2> {

	return VectorImpl<_Type, 2>(s);
}
template<Scalar _Type>
[[nodiscard]] constexpr auto VectorFillType3dOf(_Type s) -> VectorImpl<_Type, 3> {

	return VectorImpl<_Type, 3>(s);
}
template<Scalar _Type>
[[nodiscard]] constexpr auto VectorFillType4dOf(_Type s) -> VectorImpl<_Type, 4> {

	return VectorImpl<_Type, 4>(s);
}

//演算関数
template<Scalar _Type, Size _Dimension>
[[nodiscard]] constexpr auto Length(const VectorImpl<_Type, _Dimension>& v) -> ToFloatingPointType<_Type> {

	return v.Length();
}
template<Scalar _Type, Size _Dimension>
[[nodiscard]] constexpr auto Magnitude(const VectorImpl<_Type, _Dimension>& v) -> ToFloatingPointType<_Type> {

	return v.Magnitude();
}
template<Scalar _Type, Size _Dimension>
[[nodiscard]] constexpr auto Normalize(const VectorImpl<_Type, _Dimension>& v) -> VectorImpl<_Type, _Dimension> {

	return v.Normalize();
}
template<Scalar _Type, Size _Dimension>
[[nodiscard]] constexpr auto IsZero(const VectorImpl<_Type, _Dimension>& v) -> bool {

	return v.IsZero();
}
template<Scalar _Type, Size _Dimension>
[[nodiscard]] constexpr auto Inverse(const VectorImpl<_Type, _Dimension>& v) -> VectorImpl<_Type, _Dimension> {

	return -v;
}
template<Scalar _TypeL, Size _DimL, Scalar _TypeR, Size _DimR>
[[nodiscard]] constexpr auto Dot(const VectorImpl<_TypeL, _DimL>& lhv, const VectorImpl<_TypeR, _DimR>& rhv) -> GetPrecisionType<_TypeL, _TypeR> {

	return lhv.Dot(rhv);
}
template<Scalar _TypeL, Scalar _TypeR, class Ret_Type = GetPrecisionType<_TypeL, _TypeR>>
[[nodiscard]] constexpr auto Cross(const VectorImpl<_TypeL, 3>& lhv, const VectorImpl<_TypeR, 3>& rhv) -> VectorImpl<Ret_Type, 3> {

	return lhv.Cross(rhv);
}
template<Scalar _TypeL, Size _DimL, Scalar _TypeR, Size _DimR>
[[nodiscard]] constexpr auto Distance(const VectorImpl<_TypeL, _DimL>& lhv, const VectorImpl<_TypeR, _DimR>& rhv) -> GetPrecisionType<_TypeL, _TypeR> {

	return (lhv - rhv).Length();
}
template<Scalar _TypeS, Size _DimS, Scalar _TypeE, Size _DimE, Scalar _RateType, class _RetType = GetPrecisionType<_TypeS, _TypeE>, Size _RetDim = GetMaxSize<_DimS, _DimE>>
[[nodiscard]] constexpr auto Lerp(const VectorImpl<_TypeS, _DimS>& start, const VectorImpl<_TypeE, _DimE>& end, _RateType rate) -> VectorImpl<_RetType, _RetDim> {

	return start + (start - end) * rate;
}
template<Scalar _TypeL, Size _DimL, Scalar _TypeR, Size _DimR, class _RetType = GetPrecisionType<_TypeL, _TypeR>, Size _RetDim = GetMaxSize<_DimL, _DimR>>
[[nodiscard]] constexpr auto Projection(const VectorImpl<_TypeL, _DimL>& lhv, const VectorImpl<_TypeR, _DimR>& rhv) -> VectorImpl<_RetType, _RetDim> {

	return lhv * (lhv.Dot(rhv) / lhv.LengthSquare());
}
template<Scalar _TypeL, Size _DimL, Scalar _TypeR, Size _DimR, class _RetType = GetPrecisionType<_TypeL, _TypeR>, Size _RetDim = GetMaxSize<_DimL, _DimR>>
[[nodiscard]] constexpr auto Perpendicular(const VectorImpl<_TypeL, _DimL>& lhv, const VectorImpl<_TypeR, _DimR>& rhv) -> VectorImpl<_RetType, _RetDim> {

	return rhv - Projection(lhv, rhv);
}
template<Scalar _TypeF, Size _DimF, Scalar _TypeN, Size _DimN, class _RetType = GetPrecisionType<_TypeF, _TypeN>, Size _RetDim = GetMaxSize<_DimF, _DimN>>
[[nodiscard]] constexpr auto Reflect(const VectorImpl<_TypeF, _DimF>& f, const VectorImpl<_TypeN, _DimN>& n) -> VectorImpl<_RetType, _RetDim> {

	const auto& normalizedNormal = n.Normalize();
	return f + 2 * Dot(-f, normalizedNormal) * normalizedNormal;
}
template<Scalar _TypeF, Size _DimF, Scalar _TypeN, Size _DimN, Scalar _RateType, class _RetType = GetPrecisionType<_TypeF, _TypeN>, Size _RetDim = GetMaxSize<_DimF, _DimN>>
[[nodiscard]] constexpr auto Refract(const VectorImpl<_TypeF, _DimF>& f, const VectorImpl<_TypeN, _DimN>& n, _RateType eta) -> VectorImpl<_RetType, _RetDim> {

	const auto& normalizedForward = f.Normalize();
	const auto& normalizedNormal = n.Normalize();

	auto dot = Dot(normalizedForward, normalizedNormal);
	auto k = _RateType{ 1 } - Pow2(eta) * (_RateType{ 1 } - Pow2(dot));
	if (k < 0)	return {};
	return eta * normalizedForward - (eta * dot + Sqrt(k)) * normalizedNormal;
}
template<Scalar _TypeL, Size _DimL, Scalar _TypeR, Size _DimR, class _RetType = ToFloatingPointType<GetPrecisionType<_TypeL, _TypeR>>>
[[nodiscard]] constexpr auto BetweenAngle(const VectorImpl<_TypeL, _DimL>& lhv, const VectorImpl<_TypeR, _DimR>& rhv) -> RadianImpl<_RetType> {

	const auto& normalizedL = lhv.Normalize();
	const auto& normalizedR = rhv.Normalize();

	auto dot = Dot(normalizedL, normalizedR);
	return Acos(dot);
}

}
