﻿#pragma once
#include <ostream>

#include "../BaseType.hpp"
#include "../TypeTraits.hpp"

namespace Whono::Math {

template<Scalar _T, Size _Dimension>
class VectorImpl;


template<Scalar _TypeL, Size _DimL, Scalar _TypeR, Size _DimR>
[[nodiscard]] constexpr auto operator==(const VectorImpl<_TypeL, _DimL>& lhs, const VectorImpl<_TypeR, _DimR>& rhv) -> Bool {

	if (_DimL != _DimR)	return false;
	for (auto i = 0; i < _DimL; ++i) {

		if (lhs.At(i) != rhv.At(i))	return false;
	}
	return true;
}
template<Scalar _TypeL, Size _DimL, Scalar _TypeR, Size _DimR>
[[nodiscard]] constexpr auto operator!=(const VectorImpl<_TypeL, _DimL>& lhs, const VectorImpl<_TypeR, _DimR>& rhv) -> Bool {

	return !(lhs == rhv);
}

template<Scalar _TypeL, Size _DimL, Scalar _TypeR, class _RetType = VectorImpl<_TypeL, _DimL>>
[[nodiscard]] constexpr auto operator+(const VectorImpl<_TypeL, _DimL>& lhs, _TypeR rhv) -> _RetType {
	
	return _RetType(lhs) += rhv;
}
template<Scalar _TypeL, Size _DimL, Scalar _TypeR, class _RetType = VectorImpl<_TypeL, _DimL>>
[[nodiscard]] constexpr auto operator-(const VectorImpl<_TypeL, _DimL>& lhs, _TypeR rhv) -> _RetType {

	return _RetType(lhs) -= rhv;
}
template<Scalar _TypeL, Size _DimL, Scalar _TypeR, class _RetType = VectorImpl<_TypeL, _DimL>>
[[nodiscard]] constexpr auto operator*(const VectorImpl<_TypeL, _DimL>& lhs, _TypeR rhv) -> _RetType {

	return _RetType(lhs) *= rhv;
}
template<Scalar _TypeL, Size _DimL, Scalar _TypeR, class _RetType = VectorImpl<_TypeL, _DimL>>
[[nodiscard]] constexpr auto operator/(const VectorImpl<_TypeL, _DimL>& lhs, _TypeR rhv) -> _RetType {

	return _RetType(lhs) /= rhv;
}
template<Scalar _TypeL, Size _DimL, Scalar _TypeR, Size _DimR, class _RetType = VectorImpl<GetPrecisionType<_TypeL, _TypeR>, GetMaxSize<_DimL, _DimR>>>
[[nodiscard]] constexpr auto operator+(const VectorImpl<_TypeL, _DimL>& lhs, const VectorImpl<_TypeR, _DimR>& rhv) -> _RetType {
	
	return _RetType(lhs) += rhv;
}
template<Scalar _TypeL, Size _DimL, Scalar _TypeR, Size _DimR, class _RetType = VectorImpl<GetPrecisionType<_TypeL, _TypeR>, GetMaxSize<_DimL, _DimR>>>
[[nodiscard]] constexpr auto operator-(const VectorImpl<_TypeL, _DimL>& lhs, const VectorImpl<_TypeR, _DimR>& rhv) -> _RetType {

	return _RetType(lhs) -= rhv;
}
template<Scalar _TypeL, Size _DimL, Scalar _TypeR, Size _DimR, class _RetType = VectorImpl<GetPrecisionType<_TypeL, _TypeR>, GetMaxSize<_DimL, _DimR>>>
[[nodiscard]] constexpr auto operator*(const VectorImpl<_TypeL, _DimL>& lhs, const VectorImpl<_TypeR, _DimR>& rhv) -> _RetType {

	return _RetType(lhs) *= rhv;
}
template<Scalar _TypeL, Size _DimL, Scalar _TypeR, Size _DimR, class _RetType = VectorImpl<GetPrecisionType<_TypeL, _TypeR>, GetMaxSize<_DimL, _DimR>>>
[[nodiscard]] constexpr auto operator/(const VectorImpl<_TypeL, _DimL>& lhs, const VectorImpl<_TypeR, _DimR>& rhv) -> _RetType {

	return _RetType(lhs) /= rhv;
}

template<Scalar _TypeR, Size _DimR>
auto operator<<(std::ostream& os, const VectorImpl<_TypeR, _DimR>& rhv) -> std::ostream& {

	os << '{';
	for (auto i = 0u; i < _DimR; ++i) {

		os << rhv.At(i);
		if (i != _DimR - 1)	os << ", ";
	}
	os << '}';
	return os;
}
}