#pragma once
#include <array>
#include "ComponentAccess.hpp"


namespace Whono::Math {

template<Scalar _T, Size _Dimension>
class VectorImpl;

template<Scalar _T, Size _Dimension>
class CrossProduct : public ComponentAccess<_T, _Dimension> {};

template<Scalar _T>
class CrossProduct<_T, 3> : public ComponentAccess<_T, 3> {

public:
	template<Scalar _TypeR, class _RetType = GetPrecisionType<_T, _TypeR>>
	[[nodiscard]] constexpr auto Cross(const VectorImpl<_TypeR, 3>& rhv) const -> VectorImpl<_RetType, 3> {

		const auto* myself = reinterpret_cast<const _T*>(this);
		const auto* yourself = reinterpret_cast<const _TypeR*>(&rhv);
		
		std::array<_RetType, 3> cross{ 0 };
		for (auto i = 0; i < 3; ++i) {

			auto a = (i + 1) % 3;
			auto b = (i + 2) % 3;
			cross[i] = myself[a] * yourself[b] - myself[b] * yourself[a];
		}
		return VectorImpl<_RetType, 3>(cross);
	}
};


}
