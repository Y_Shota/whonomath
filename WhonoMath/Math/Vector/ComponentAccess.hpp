#pragma once
#include "../TypeTraits.hpp"

namespace Whono::Math {

template<Scalar _T, Size _Size>
class ComponentAccess;

template<Scalar _T>
class ComponentAccess<_T, 1> {

public:
	[[nodiscard]] auto X() -> _T& {

		return reinterpret_cast<_T*>(this)[0];
	}
	void X(_T x) {

		reinterpret_cast<_T*>(this)[0] = x;
	}
	[[nodiscard]] constexpr auto X() const -> const _T& {

		return reinterpret_cast<const _T*>(this)[0];
	}
};

template<Scalar _T>
class ComponentAccess<_T, 2> : public ComponentAccess<_T, 1> {

public:
	[[nodiscard]] auto Y() -> _T& {

		return reinterpret_cast<_T*>(this)[1];
	}
	void Y(_T y) {

		reinterpret_cast<_T*>(this)[1] = y;
	}
	[[nodiscard]] constexpr auto Y() const -> const _T& {

		return reinterpret_cast<const _T*>(this)[1];
	}
};

template<Scalar _T>
class ComponentAccess<_T, 3> : public ComponentAccess<_T, 2> {

public:
	[[nodiscard]] auto Z() -> _T& {

		return reinterpret_cast<_T*>(this)[2];
	}
	void Z(_T z) {

		reinterpret_cast<_T*>(this)[2] = z;
	}
	[[nodiscard]] constexpr auto Z() const -> const _T& {

		return reinterpret_cast<const _T*>(this)[2];
	}
};

template<Scalar _T>
class ComponentAccess<_T, 4> : public ComponentAccess<_T, 3> {

public:
	[[nodiscard]] auto W() -> _T& {

		return reinterpret_cast<_T*>(this)[3];
	}
	void W(_T w) {

		reinterpret_cast<_T*>(this)[3] = w;
	}
	[[nodiscard]] constexpr auto W() const -> const _T& {

		return reinterpret_cast<const _T*>(this)[3];
	}
};

template<Size _Size, Scalar _T>
class ComponentAccess<_T, _Size> : public ComponentAccess<_T, 4> {};

}