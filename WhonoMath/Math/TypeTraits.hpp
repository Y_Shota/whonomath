#pragma once
#include <type_traits>

#include "BaseType.hpp"

namespace Whono::Math {

template<class _T>
struct IsIntegral : std::false_type {};
template<> struct IsIntegral<char> : std::true_type {};
template<> struct IsIntegral<unsigned char> : std::true_type {};
template<> struct IsIntegral<signed char> : std::true_type {};
template<> struct IsIntegral<short> : std::true_type {};
template<> struct IsIntegral<unsigned short> : std::true_type {};
template<> struct IsIntegral<int> : std::true_type {};
template<> struct IsIntegral<unsigned int> : std::true_type {};
template<> struct IsIntegral<long> : std::true_type {};
template<> struct IsIntegral<unsigned long> : std::true_type {};
template<> struct IsIntegral<long long> : std::true_type {};
template<> struct IsIntegral<unsigned long long> : std::true_type {};
template<class _T>
constexpr auto IsIntegralValue = IsIntegral<_T>::value;
template<class _T>
concept Integral = IsIntegralValue<_T>;

template<class _T>
struct IsFloatingPoint : std::false_type {};
template<> struct IsFloatingPoint<Float> : std::true_type {};
template<> struct IsFloatingPoint<Double> : std::true_type {};
template<> struct IsFloatingPoint<long double> : std::true_type {};
template<class _T>
constexpr auto IsFloatingPointValue = IsFloatingPoint<_T>::value;
template<class _T>
concept FloatingPoint = IsFloatingPointValue<_T>;

template<class _T>
struct IsScalar : std::false_type {};
template<> struct IsScalar<char> : std::true_type {};
template<> struct IsScalar<unsigned char> : std::true_type {};
template<> struct IsScalar<signed char> : std::true_type {};
template<> struct IsScalar<short> : std::true_type {};
template<> struct IsScalar<unsigned short> : std::true_type {};
template<> struct IsScalar<int> : std::true_type {};
template<> struct IsScalar<unsigned int> : std::true_type {};
template<> struct IsScalar<long> : std::true_type {};
template<> struct IsScalar<unsigned long> : std::true_type {};
template<> struct IsScalar<long long> : std::true_type {};
template<> struct IsScalar<unsigned long long> : std::true_type {};
template<> struct IsScalar<Float> : std::true_type {};
template<> struct IsScalar<Double> : std::true_type {};
template<> struct IsScalar<long double> : std::true_type {};
template<class _T>
constexpr auto IsScalarValue = IsScalar<_T>::value;
template<class _T>
concept Scalar = IsScalarValue<_T>;


template <Scalar _T>
struct ToFloatingPoint { using Type = BaseFloatingPoint; };
template<FloatingPoint _T>
struct ToFloatingPoint<_T> {	using Type = _T; };
template<Scalar _T>
using ToFloatingPointType = typename ToFloatingPoint<_T>::Type;

template<Scalar _T, Scalar _U>
struct GetPrecision { using Type = decltype(_T(0) + _U(0)); };
template<Scalar _T, Scalar _U>
using GetPrecisionType = typename GetPrecision<_T, _U>::Type;

template<Size _L, Size _R>
struct GetMax { static constexpr auto SIZE = _L > _R ? _L : _R; };
template<Size _L, Size _R>
constexpr auto GetMaxSize = GetMax<_L, _R>::SIZE;

template<Size _L, Size _R>
struct GetMin { static constexpr auto SIZE = _L < _R ? _L : _R; };
template<Size _L, Size _R>
constexpr auto GetMinSize = GetMin<_L, _R>::SIZE;

}
