#pragma once
#include "Implement.hpp"
#include "../TypeTraits.hpp"
#include "../Angle/Function.hpp"


namespace Whono::Math {

template<Size _Row, Size _Column, Scalar... _Args>
requires (sizeof...(_Args) == _Row * _Column)
[[nodiscard]] constexpr auto MatrixOf(_Args... args) -> MatrixImpl<BaseFloatingPoint, _Row, _Column> {

	return { args... };
}
[[nodiscard]] constexpr auto Matrix2dOf(
	BaseFloatingPoint m00, BaseFloatingPoint m01, BaseFloatingPoint m02,
	BaseFloatingPoint m10, BaseFloatingPoint m11, BaseFloatingPoint m12,
	BaseFloatingPoint m20, BaseFloatingPoint m21, BaseFloatingPoint m22
) -> MatrixImpl<BaseFloatingPoint, 3> {

	return {
		m00, m01, m02,
		m10, m11, m12,
		m20, m21, m22 };
}
[[nodiscard]] constexpr auto Matrix3Of(
	BaseFloatingPoint m00, BaseFloatingPoint m01, BaseFloatingPoint m02, BaseFloatingPoint m03,
	BaseFloatingPoint m10, BaseFloatingPoint m11, BaseFloatingPoint m12, BaseFloatingPoint m13,
	BaseFloatingPoint m20, BaseFloatingPoint m21, BaseFloatingPoint m22, BaseFloatingPoint m23,
	BaseFloatingPoint m30, BaseFloatingPoint m31, BaseFloatingPoint m32, BaseFloatingPoint m33
) -> MatrixImpl<BaseFloatingPoint, 4> {

	return {
		m00, m01, m02, m03,
		m10, m11, m12, m13,
		m20, m21, m22, m23,
		m30, m31, m32, m33 };
}
[[nodiscard]] constexpr auto Matrix4Of(
	BaseFloatingPoint m00, BaseFloatingPoint m01, BaseFloatingPoint m02, BaseFloatingPoint m03, BaseFloatingPoint m04,
	BaseFloatingPoint m10, BaseFloatingPoint m11, BaseFloatingPoint m12, BaseFloatingPoint m13, BaseFloatingPoint m14,
	BaseFloatingPoint m20, BaseFloatingPoint m21, BaseFloatingPoint m22, BaseFloatingPoint m23, BaseFloatingPoint m24,
	BaseFloatingPoint m30, BaseFloatingPoint m31, BaseFloatingPoint m32, BaseFloatingPoint m33, BaseFloatingPoint m34,
	BaseFloatingPoint m40, BaseFloatingPoint m41, BaseFloatingPoint m42, BaseFloatingPoint m43, BaseFloatingPoint m44
) -> MatrixImpl<BaseFloatingPoint, 5> {

	return {
		m00, m01, m02, m03, m04,
		m10, m11, m12, m13, m14,
		m20, m21, m22, m23, m24,
		m30, m31, m32, m33, m34,
		m40, m41, m42, m43, m44 };
}
template<Scalar _Type, Size _Row, Size _Column, Scalar... _Args>
requires (sizeof...(_Args) == _Row * _Column)
[[nodiscard]] constexpr auto MatrixOf(const _Args&... args) -> MatrixImpl<_Type, _Row, _Column> {

	return { args... };
}
template<Scalar _Type, Size _Size, Scalar... _Args>
requires (sizeof...(_Args) == _Size * _Size)
[[nodiscard]] constexpr auto MatrixSquareOf(const _Args&... args) -> MatrixImpl<_Type, _Size, _Size> {

	return MatrixOf<_Type, _Size, _Size>(args);
}
template<Size _Row, Size _Column, Scalar... _Args>
requires (sizeof...(_Args) == _Row * _Column)
[[nodiscard]] constexpr auto MatrixOf(const _Args&... args) -> MatrixImpl<BaseFloatingPoint, _Row, _Column> {

	return { args... };
}
template<Size _Size, Scalar... _Args>
requires (sizeof...(_Args) == _Size * _Size)
[[nodiscard]] constexpr auto MatrixSquareOf(const _Args&... args) -> MatrixImpl<BaseFloatingPoint, _Size, _Size> {

	return MatrixOf<_Size, _Size>(args);
}
template<Scalar _Type>
[[nodiscard]] constexpr auto Matrix2dOf(
	_Type m00, _Type m01, _Type m02,
	_Type m10, _Type m11, _Type m12,
	_Type m20, _Type m21, _Type m22
) -> MatrixImpl<_Type, 3> {

	return {
		m00, m01, m02,
		m10, m11, m12,
		m20, m21, m22 };
}
template<Scalar _Type>
[[nodiscard]] constexpr auto Matrix3dOf(
	_Type m00, _Type m01, _Type m02, _Type m03,
	_Type m10, _Type m11, _Type m12, _Type m13,
	_Type m20, _Type m21, _Type m22, _Type m23,
	_Type m30, _Type m31, _Type m32, _Type m33
) -> MatrixImpl<BaseFloatingPoint, 4> {

	return {
		m00, m01, m02, m03,
		m10, m11, m12, m13,
		m20, m21, m22, m23,
		m30, m31, m32, m33 };
}
template<Scalar _Type>
[[nodiscard]] constexpr auto Matrix4dOf(
	_Type m00, _Type m01, _Type m02, _Type m03, _Type m04,
	_Type m10, _Type m11, _Type m12, _Type m13, _Type m14,
	_Type m20, _Type m21, _Type m22, _Type m23, _Type m24,
	_Type m30, _Type m31, _Type m32, _Type m33, _Type m34,
	_Type m40, _Type m41, _Type m42, _Type m43, _Type m44
) -> MatrixImpl<BaseFloatingPoint, 5> {

	return {
		m00, m01, m02, m03, m04,
		m10, m11, m12, m13, m14,
		m20, m21, m22, m23, m24,
		m30, m31, m32, m33, m34,
		m40, m41, m42, m43, m44 };
}
//単位行列
template<Scalar _Type, Size _Size>
[[nodiscard]] constexpr auto MatrixIdentity() -> MatrixImpl<_Type, _Size, _Size> {

	return MethodSquare<_Type, _Size, _Size>::Identity();
}
template<Size _Size>
[[nodiscard]] constexpr auto MatrixIdentity() -> MatrixImpl<BaseFloatingPoint, _Size, _Size> {

	return MethodSquare<BaseFloatingPoint, _Size, _Size>::Identity();
}
template<Scalar _Type>
[[nodiscard]] constexpr auto Matrix2dIdentity() -> MatrixImpl<_Type, 3, 3> {

	return MatrixIdentity<_Type, 3>();
}
[[nodiscard]] constexpr auto Matrix2dIdentity() -> MatrixImpl<BaseFloatingPoint, 3, 3> {

	return Matrix2dIdentity<BaseFloatingPoint>();
}
template<Scalar _Type>
[[nodiscard]] constexpr auto Matrix3dIdentity() -> MatrixImpl<_Type, 4, 4> {

	return MatrixIdentity<_Type, 4>();
}
[[nodiscard]] constexpr auto Matrix3dIdentity() -> MatrixImpl<BaseFloatingPoint, 4, 4> {

	return Matrix3dIdentity<BaseFloatingPoint>();
}
template<Scalar _Type>
[[nodiscard]] constexpr auto Matrix4dIdentity() -> MatrixImpl<_Type, 5, 5> {

	return MatrixIdentity<_Type, 5>();
}
[[nodiscard]] constexpr auto Matrix4dIdentity() -> MatrixImpl<BaseFloatingPoint, 5, 5> {

	return Matrix4dIdentity<BaseFloatingPoint>();
}

//アフィン変換行列
//2d
template<Scalar _Type>
[[nodiscard]] constexpr auto Matrix2dTranslation(const VectorImpl<_Type, 2>& translate) -> MatrixImpl<_Type, 3> {

	return Matrix2dIdentity()
		.SetRow(2, translate)
		.Set(2, 2, 1);
}
[[nodiscard]] constexpr auto Matrix2dTranslation(const VectorImpl<BaseFloatingPoint, 2>& translate) -> MatrixImpl<BaseFloatingPoint, 3> {

	return Matrix2dTranslation<BaseFloatingPoint>(translate);
}
template<Scalar _Type>
[[nodiscard]] constexpr auto Matrix2dTranslation(const _Type& x, const _Type& y) -> MatrixImpl<_Type, 3> {

	return Matrix2dTranslation({ x, y });
}
[[nodiscard]] constexpr auto Matrix2dTranslation(BaseFloatingPoint x, BaseFloatingPoint y) -> MatrixImpl<BaseFloatingPoint, 3> {

	return Matrix2dTranslation<BaseFloatingPoint>( x, y );
}
template<Scalar _Type>
[[nodiscard]] constexpr auto Matrix2dRotation(const RadianImpl<_Type>& angle) -> MatrixImpl<_Type, 3> {

	auto sinCos = SinCos(angle);
	return Matrix2dIdentity()
		.SetRow(0, { sinCos.second, -sinCos.first, 0 })
		.SetRow(0, { sinCos.first,	sinCos.second, 0 });
}
[[nodiscard]] constexpr auto Matrix2dRotation(const RadianImpl<BaseFloatingPoint>& angle) -> MatrixImpl<BaseFloatingPoint, 3> {
	
	return Matrix2dRotation<BaseFloatingPoint>(angle);
}
template<Scalar _Type>
[[nodiscard]] constexpr auto Matrix2dScaling(const VectorImpl<_Type, 2>& scale) -> MatrixImpl<_Type, 3> {

	return Matrix2dIdentity()
		.Set(0, 0, scale.X())
		.Set(1, 1, scale.Y());
}
[[nodiscard]] constexpr auto Matrix2dScaling(const VectorImpl<BaseFloatingPoint, 2>& scale) -> MatrixImpl<BaseFloatingPoint, 3> {

	return Matrix2dScaling<BaseFloatingPoint>(scale);
}
template<Scalar _Type>
[[nodiscard]] constexpr auto Matrix2dScaling(const _Type& x, const _Type& y) -> MatrixImpl<_Type, 3> {

	return Matrix2dScaling({ x, y });
}
[[nodiscard]] constexpr auto Matrix2dScaling(BaseFloatingPoint x, BaseFloatingPoint y) -> MatrixImpl<BaseFloatingPoint, 3> {

	return Matrix2dScaling<BaseFloatingPoint>(x, y);
}
//3d
template<class _Type>
[[nodiscard]] constexpr auto Matrix3dTranslation(const VectorImpl<_Type, 3>& translate) -> MatrixImpl<_Type, 4> {

	return Matrix3dIdentity()
		.SetRow(3, translate)
		.Set(3, 3, 1);
}
[[nodiscard]] constexpr auto Matrix3dTranslation(const VectorImpl<BaseFloatingPoint, 3>& translate) ->MatrixImpl<BaseFloatingPoint, 4> {

	return Matrix3dTranslation<BaseFloatingPoint>(translate);
}
template<class _Type>
[[nodiscard]] constexpr auto Matrix3dRotationX(const RadianImpl<_Type>& x) -> MatrixImpl<_Type, 4> {

	auto sinCos = SinCos(x);
	return Matrix3dIdentity()
		.SetRow(1, { 0, sinCos.second, sinCos.first, 0 })
		.SetRow(2, { 0,	-sinCos.first, sinCos.second, 0 });
}
[[nodiscard]] constexpr auto Matrix3dRotationX(const RadianImpl<BaseFloatingPoint>& x) -> MatrixImpl<BaseFloatingPoint, 4> {

	return Matrix3dRotationX<BaseFloatingPoint>(x);
}
template<class _Type>
[[nodiscard]] constexpr auto Matrix3dRotationY(const RadianImpl<_Type>& y) -> MatrixImpl<_Type, 4> {

	auto sinCos = SinCos(y);
	return Matrix3dIdentity()
		.SetRow(0, { sinCos.second, 0, -sinCos.first, 0 })
		.SetRow(2, { sinCos.first, 0, sinCos.second, 0 });
}
[[nodiscard]] constexpr auto Matrix3dRotationY(const RadianImpl<BaseFloatingPoint>& y) -> MatrixImpl<BaseFloatingPoint, 4> {

	return Matrix3dRotationY<BaseFloatingPoint>(y);
}
template<class _Type>
[[nodiscard]] constexpr auto Matrix3dRotationZ(const RadianImpl<_Type>& z) -> MatrixImpl<_Type, 4> {

	auto sinCos = SinCos(z);
	return Matrix3dIdentity()
		.SetRow(0, { sinCos.second, sinCos.first, 0, 0 })
		.SetRow(1, { -sinCos.first, sinCos.second, 0, 0 });
}
[[nodiscard]] constexpr auto Matrix3dRotationZ(const RadianImpl<BaseFloatingPoint>& z) -> MatrixImpl<BaseFloatingPoint, 4> {

	return Matrix3dRotationZ<BaseFloatingPoint>(z);
}
template<class _Type>
[[nodiscard]] constexpr auto Matrix3dRotation(const RadianImpl<_Type>& x, const RadianImpl<_Type>& y, const RadianImpl<_Type>& z) -> MatrixImpl<_Type, 4> {

	return Matrix3dRotationZ(z) *= Matrix3dRotationX(x) *= Matrix3dRotationY(y);
}
[[nodiscard]] constexpr auto Matrix3dRotation(const RadianImpl<BaseFloatingPoint>& x, const RadianImpl<BaseFloatingPoint>& y, const RadianImpl<BaseFloatingPoint>& z) -> MatrixImpl<BaseFloatingPoint, 4> {

	return Matrix3dRotation(x, y, z);
}
template<class _Type>
[[nodiscard]] constexpr auto Matrix3dScaling(const VectorImpl<_Type, 3>& scale) -> MatrixImpl<_Type, 4> {

	return Matrix3dIdentity()
		.Set(0, 0, scale.X())
		.Set(1, 1, scale.Y())
		.Set(2, 2, scale.Z());
}
[[nodiscard]] constexpr auto Matrix3dScaling(const VectorImpl<BaseFloatingPoint, 3>& scale) -> MatrixImpl<BaseFloatingPoint, 4> {

	return Matrix3dScaling<BaseFloatingPoint>(scale);
}
template<class _Type>
[[nodiscard]] constexpr auto Matrix3dScaling(const _Type& x, const _Type& y, const _Type& z) -> MatrixImpl<_Type, 4> {

	return Matrix3dScaling({ x, y, z });
}
[[nodiscard]] constexpr auto Matrix3dScaling(BaseFloatingPoint x, BaseFloatingPoint y, BaseFloatingPoint z) -> MatrixImpl<BaseFloatingPoint, 4> {

	return Matrix3dScaling(x, y, z);
}

}