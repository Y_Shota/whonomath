#pragma once
#include <ostream>

#include "../BaseType.hpp"
#include "../TypeTraits.hpp"

namespace Whono::Math {

template<Scalar _T, Size _Row, Size _Column>
class MatrixImpl;

template<Scalar _TypeL, Size _RowL, Size _ColumnL, Scalar _TypeR, Size _RowR, Size _ColumnR>
[[nodiscard]] constexpr auto operator==(const MatrixImpl<_TypeL, _RowL, _ColumnL>& lhv, const MatrixImpl<_TypeR, _RowR, _ColumnR>& rhv) -> Bool {

	if (_RowL != _RowR)			return false;
	if (_ColumnL != _ColumnR)	return false;

	for (auto i = 0u; i < _RowL; ++i) {

		if (lhv.GetRow(i) != rhv.GetRow(i))	return false;
	}
	return true;
}
template<Scalar _TypeL, Size _RowL, Size _ColumnL, Scalar _TypeR, Size _RowR, Size _ColumnR>
[[nodiscard]] constexpr auto operator!=(const MatrixImpl<_TypeL, _RowL, _ColumnL>& lhv, const MatrixImpl<_TypeR, _RowR, _ColumnR>& rhv) -> Bool {

	return !(lhv == rhv);
}

template<Scalar _TypeL, Size _RowL, Size _ColumnL, Scalar _TypeR, class _RetType = MatrixImpl<_TypeL, _RowL, _ColumnL>>
[[nodiscard]] constexpr auto operator+(const MatrixImpl<_TypeL, _RowL, _ColumnL>& lhv, _TypeR rhv) -> _RetType {
	
	return _RetType(lhv) += rhv;
}
template<Scalar _TypeL, Size _RowL, Size _ColumnL, Scalar _TypeR, class _RetType = MatrixImpl<_TypeL, _RowL, _ColumnL>>
[[nodiscard]] constexpr auto operator-(const MatrixImpl<_TypeL, _RowL, _ColumnL>& lhv, _TypeR rhv) -> _RetType {

	return _RetType(lhv) -= rhv;
}
template<Scalar _TypeL, Size _RowL, Size _ColumnL, Scalar _TypeR, class _RetType = MatrixImpl<_TypeL, _RowL, _ColumnL>>
[[nodiscard]] constexpr auto operator*(const MatrixImpl<_TypeL, _RowL, _ColumnL>& lhv, _TypeR rhv) -> _RetType {

	return _RetType(lhv) *= rhv;
}
template<Scalar _TypeL, Size _RowL, Size _ColumnL, Scalar _TypeR, class _RetType = MatrixImpl<_TypeL, _RowL, _ColumnL>>
[[nodiscard]] constexpr auto operator/(const MatrixImpl<_TypeL, _RowL, _ColumnL>& lhv, _TypeR rhv) -> _RetType {

	return _RetType(lhv) /= rhv;
}
template<Scalar _Type, Size _Row, Size _Column, class _RetType = MatrixImpl<_Type, _Row, _Column>>
[[nodiscard]] constexpr auto operator+(const MatrixImpl<_Type, _Row, _Column>& lhv, const MatrixImpl<_Type, _Row, _Column>& rhv) -> _RetType {

	return _RetType(lhv) += rhv;
}
template<Scalar _Type, Size _Row, Size _Column, class _RetType = MatrixImpl<_Type, _Row, _Column>>
[[nodiscard]] constexpr auto operator-(const MatrixImpl<_Type, _Row, _Column>& lhv, const MatrixImpl<_Type, _Row, _Column>& rhv) -> _RetType {

	return _RetType(lhv) -= rhv;
}
template<Scalar _TypeL, Size _RowL, Size _ColumnL, Scalar _TypeR, Size _RowR, Size _ColumnR, class _RetType = MatrixImpl<_TypeL, _RowR, _ColumnL>>
requires (_RowR == _ColumnL && _ColumnR == _RowL)
[[nodiscard]] constexpr auto operator*(const MatrixImpl<_TypeL, _RowL, _ColumnL>& lhv, const MatrixImpl<_TypeR, _RowR, _ColumnR>& rhv) -> _RetType {

	return _RetType(lhv) *= rhv;
}

template<Scalar _TypeR, Size _RowR, Size _ColumnR>
auto operator<<(std::ostream& os, const MatrixImpl<_TypeR, _RowR, _ColumnR>& rhv) -> std::ostream& {

	os << '{';
	for (auto i = 0u; i < _RowR; ++i) {

		os << rhv.GetRow(i);
		if (i != _RowR - 1)	os << ", ";
	}
	os << '}';
	return os;
}
}