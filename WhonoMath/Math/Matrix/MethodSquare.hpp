#pragma once

#include "TransformAccess.hpp"

namespace Whono::Math {

template<Scalar _T, Size _Row, Size _Column>
class MatrixImpl;

template<Scalar _T, Size _Row, Size _Column>
class MethodSquare : TransformAccess<_T, _Row, _Column> {

public:
	[[nodiscard]] static constexpr auto IsSquare() -> Bool {

		return false;
	}
};

template<Scalar _T, Size _Size>
class MethodSquare<_T, _Size, _Size> : TransformAccess<_T, _Size, _Size> {

private:
	using SelfType = MatrixImpl<_T, _Size, _Size>;

public:
	[[nodiscard]] constexpr auto Determinant() const -> _T {

		const auto& self = reinterpret_cast<const SelfType&>(*this);
		return Det(self);
	}
	[[nodiscard]] static constexpr auto IsSquare() -> Bool {

		return true;
	}
	[[nodiscard]] constexpr auto IsRegular() const -> Bool {

		return 0 != Det(*this);
	}
	[[nodiscard]] constexpr auto Adjugate() const -> SelfType {

		SelfType buf{};
		const auto& self = reinterpret_cast<const SelfType&>(*this);
		for (auto i = 0u; i < _Size; ++i) {
			for (auto j = 0u; j < _Size; ++j) {

				buf(j, i) = Det(self.Cut(i, j));
			}
		}
		return buf;
	}
	[[nodiscard]] constexpr auto Inverse() const -> SelfType {
		
		auto det = Determinant();
		if (det == 0) {
			
			return SelfType(std::numeric_limits<_T>::quiet_NaN());
		}
		return Adjugate() / det;
	}
	[[nodiscard]] constexpr auto Trace() const -> _T {

		const auto& self = reinterpret_cast<const SelfType&>(*this);

		auto buf = _T{ 0 };
		for (auto i = 0u; i < _Size; ++i)	buf += self.At(i, i);

		return buf;
	}
	[[nodiscard]] constexpr auto Dot(const SelfType& rhv) const -> _T {

		const auto& self = reinterpret_cast<const SelfType&>(*this);

		auto buf = _T{ 0 };
		for (auto i = 0u; i < _Size; ++i) {
			for (auto j = 0u; j < _Size; ++j) {

				buf += self.At(i, j) * rhv.At(i, j);
			}
		}

		return buf;
	}
	[[nodiscard]] static constexpr auto Identity() -> SelfType {

		auto buf = SelfType{};
		for (auto i = 0u; i < _Size; ++i) {

			buf[i][i] = _T{ 0 };
		}
		return buf;
	}

private:
	template<Scalar _U>
	[[nodiscard]] static constexpr auto Det(const MatrixImpl<_U, 2, 2>& m) -> _U {

		return m(0, 0) * m(1, 1) - m(1, 0) * m(0, 1);
	}
	template<Scalar _U, Size _S>
	[[nodiscard]] static constexpr auto Det(const MatrixImpl<_U, _S, _S>& m) -> _U {

		auto buf = _U{ 0 };
		for (auto i = 0u; i < _S; ++i) {

			buf += (i % 2 ? -1 : 1) * m(0, i) * Det(m.Cut(0, i));
		}
		return buf;
	}
};





}
