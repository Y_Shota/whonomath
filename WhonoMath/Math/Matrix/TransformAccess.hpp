#pragma once
#include "../TypeTraits.hpp"
#include "../Vector/Implement.hpp"

//todo

namespace Whono::Math {

template<Scalar _T, Size _Row, Size _Column>
class TransformAccess {};

template<Scalar _T>
class TransformAccess<_T, 4, 4> {
	
public:
	using ValueType = _T;
	using VectorType = VectorImpl<ValueType, 4>;
	using TranslationType = VectorImpl<ValueType, 3>;
	//using RotationType = VectorImpl<ValueType, 3>;
	//using OrientationType = Quaternion<ValueType>;
	using ScaleType = VectorImpl<ValueType, 3>;
	
	[[nodiscard]] constexpr auto GetTranslation() const -> TranslationType {

		const auto* myself = reinterpret_cast<const VectorType*>(this);
		return myself[3];
	}
	/*[[nodiscard]] constexpr auto GetRotation() const -> RotationType {

	}
	[[nodiscard]] constexpr auto GetOrientation() const -> OrientationType {
		
	}
	[[nodiscard]] constexpr auto GetScale() const -> ScaleType {

	}*/
};

template<Scalar _T>
class TransformAccess<_T, 4, 3> {

public:
	using DataType = _T;
	using VectorType = VectorImpl<DataType, 3>;
	using TranslationType = VectorImpl<DataType, 3>;
	//using RotationType = VectorImpl<ValueType, 3>;
	//using OrientationType = Quaternion<ValueType>;
	using ScaleType = VectorImpl<DataType, 3>;

	[[nodiscard]] constexpr auto GetTranslation() const -> TranslationType {

		const auto* myself = reinterpret_cast<const VectorType*>(this);
		return myself[3];
	}
	/*[[nodiscard]] constexpr auto GetRotation() const -> RotationType {

	}
	[[nodiscard]] constexpr auto GetOrientation() const -> OrientationType {

	}
	[[nodiscard]] constexpr auto GetScale() const -> ScaleType {

	}*/
};


//3x3 or 3x2 2d
template<Scalar _T>
class TransformAccess<_T, 3, 3> {

public:
	using DataType = _T;
	using VectorType = VectorImpl<DataType, 3>;
	using TranslationType = VectorImpl<DataType, 2>;
	//using RotationType = AngleImpl<ValueType>;
	//using OrientationType = Complex<ValueType>;
	using ScaleType = VectorImpl<DataType, 2>;

	[[nodiscard]] constexpr auto GetTranslation() const -> TranslationType {

		const auto* myself = reinterpret_cast<const VectorType*>(this);
		return myself[2];
	}
	/*[[nodiscard]] constexpr auto GetRotation() const -> RotationType {

	}
	[[nodiscard]] constexpr auto GetOrientation() const -> OrientationType {

	}
	[[nodiscard]] constexpr auto GetScale() const -> ScaleType {

	}*/
};
template<Scalar _T>
class TransformAccess<_T, 3, 2> {

public:
	using DataType = _T;
	using VectorType = VectorImpl<DataType, 2>;
	using TranslationType = VectorImpl<DataType, 2>;
	//using RotationType = AngleImpl<ValueType>;
	//using OrientationType = Complex<ValueType>;
	using ScaleType = VectorImpl<DataType, 2>;

	[[nodiscard]] constexpr auto GetTranslation() const -> TranslationType {

		const auto* myself = reinterpret_cast<const VectorType*>(this);
		return myself[2];
	}
	/*[[nodiscard]] constexpr auto GetRotation() const -> RotationType {

	}
	[[nodiscard]] constexpr auto GetOrientation() const -> OrientationType {

	}
	[[nodiscard]] constexpr auto GetScale() const -> ScaleType {

	}*/
};

}
