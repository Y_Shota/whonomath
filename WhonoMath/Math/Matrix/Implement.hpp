#pragma once
#include <cassert>

#include "MethodSquare.hpp"


namespace Whono::Math {

template<Scalar _T, Size _Row, Size _Column = _Row>
class MatrixImpl : public MethodSquare<_T, _Row, _Column> {

public:
	static constexpr auto ROW = _Row;
	static constexpr auto COLUMN = _Column;
	static constexpr auto ELEM_SIZE = ROW * COLUMN;

	using ValueType = _T;
	using RValueType = _T&;
	using CRValueType = const _T&;
	using LValueType = _T&&;
	using PValueType = _T*;
	using CPValueType = const _T*;
	using FloatingValueType = ToFloatingPointType<_T>;
	
	using VectorType = VectorImpl<ValueType, COLUMN>;
	using RVectorType = VectorType&;
	using CRVectorType = const VectorType&;
	using RowVector = VectorType;
	using ColumnVector = VectorImpl<ValueType, ROW>;

private:
	union {
		ValueType rawData_[ROW][COLUMN];
		ValueType rawDataAll[ELEM_SIZE];
		std::array<VectorType, ROW> data_;
		std::array<ValueType, ELEM_SIZE> dataAll_;
	};

public:
	[[nodiscard]] constexpr MatrixImpl()
		: data_{} {}
	[[nodiscard]] constexpr MatrixImpl(std::initializer_list<VectorType> initList)
		: MatrixImpl(initList.begin(), initList.size(), std::make_index_sequence<ROW>()) {}
	[[nodiscard]] constexpr explicit MatrixImpl(ValueType n)
		: MatrixImpl() {

		Fill(n);
	}
	template<Scalar... _U>
	requires (sizeof...(_U) == ROW * COLUMN)
	[[nodiscard]] constexpr MatrixImpl(const _U&... init)
		: dataAll_{ static_cast<ValueType>(init)... } {}
	template<Scalar _TypeR, Size _RowR, Size _ColumnR>
	requires (ROW >= _RowR && COLUMN >= _ColumnR)
	[[nodiscard]] constexpr MatrixImpl(const MatrixImpl<_TypeR, _RowR, _ColumnR>& rhv)
		: MatrixImpl() {

		for (auto i = 0u; i < _RowR; ++i) {

			SetRow(i, rhv.GetRow(i));
		}
	}
	template<Scalar _TypeR, Size _RowR, Size _ColumnR>
	requires (ROW >= _RowR && COLUMN >= _ColumnR)
	[[nodiscard]] constexpr MatrixImpl(const _TypeR(&rhv)[_RowR][_ColumnR])
		: MatrixImpl() {

		for (auto i = 0u; i < _RowR; ++i) {
			for (auto j = 0u; j < _ColumnR; ++j) {

				data_[i][j] = static_cast<ValueType>(rhv[i][j]);
			}
		}
	}

	[[nodiscard]] constexpr MatrixImpl(const MatrixImpl&) = default;
	auto operator=(const MatrixImpl&) -> MatrixImpl& = default;
	[[nodiscard]] constexpr MatrixImpl(MatrixImpl&&) = default;
	auto operator=(MatrixImpl&&) -> MatrixImpl& = default;

private:
	template<Size... _Indices>
	[[nodiscard]] constexpr MatrixImpl(const VectorType* data, Size size, std::index_sequence<_Indices...>)
		: data_{ Apply(data, size, _Indices)... } {

		assert(size <= ROW);
	}

	static constexpr auto Apply(const VectorType* data, Size size, Size index) -> VectorType {

		if (index >= size)	return VectorType{};
		return *(data + index);
	}

public:
	[[nodiscard]] auto Data() -> VectorType* {

		return data_.data();
	}
	[[nodiscard]] constexpr auto Data() const -> VectorType const* {

		return data_.data();
	}
	
	[[nodiscard]] auto At(Size i, Size j) & noexcept(false) -> RValueType {

		return data_.at(i).At(j);
	}
	[[nodiscard]] constexpr auto At(Size i, Size j) const& noexcept(false) -> CRValueType {

		return data_.at(i).At(j);
	}
	[[nodiscard]] constexpr auto At(Size i, Size j) const&& noexcept(false) -> ValueType {

		return std::move(data_.at(i).At(j));
	}
	[[nodiscard]] auto operator()(Size i, Size j) & noexcept(false) -> RValueType {

		return data_[i][j];
	}
	[[nodiscard]] constexpr auto operator()(Size i, Size j) const& noexcept(false) -> CRValueType {

		return data_[i][j];
	}
	[[nodiscard]] constexpr auto operator()(Size i, Size j) const&& noexcept(false) -> ValueType {

		return std::move(data_[i][j]);
	}
	[[nodiscard]] auto operator[](Size i) & noexcept(false) -> RVectorType {

		return data_[i];
	}
	[[nodiscard]] constexpr auto operator[](Size i) const& noexcept(false) -> CRVectorType {

		return data_[i];
	}
	[[nodiscard]] constexpr auto operator[](Size i) && noexcept(false) -> VectorType {

		return std::move(data_[i]);
	}
	
	[[nodiscard]] constexpr auto GetRow(Size index) const& noexcept(false) -> const RowVector& {

		return data_.at(index);
	}
	[[nodiscard]] constexpr auto GetRow(Size index) const&& noexcept(false) -> RowVector {

		return std::move(data_.at(index));
	}
	[[nodiscard]] constexpr auto GetColumn(Size index) const noexcept(false) -> ColumnVector {

		ColumnVector buf;
		for (auto i = 0u; i < index; ++i) {

			buf[i] = data_.at(i)[index];
		}
		return buf;
	}

	auto Set(Size i, Size j, ValueType value) & noexcept(false) -> MatrixImpl& {

		data_[i][j] = value;
		return *this;
	}
	auto Set(Size i, Size j, ValueType value) && noexcept(false) -> MatrixImpl {

		data_[i][j] = value;
		return std::move(*this);
	}
	auto SetRow(Size index, const RowVector& row) & noexcept(false) -> MatrixImpl& {

		data_[index] = row;
		return *this;
	}
	auto SetRow(Size index, const RowVector& row) && noexcept(false) -> MatrixImpl {

		data_[index] = row;
		return std::move(*this);
	}
	auto SetColumn(Size index, const ColumnVector& column) & noexcept(false) -> MatrixImpl& {

		for (auto i = 0; i < COLUMN; ++i) {

			data_[i][index] = column[i];
		}
		return *this;
	}
	auto SetColumn(Size index, const ColumnVector& column) && noexcept(false) -> MatrixImpl {

		for (auto i = 0; i < COLUMN; ++i) {

			data_[i][index] = column[i];
		}
		return std::move(*this);
	}
	auto Fill(ValueType s) & -> MatrixImpl& {

		data_.fill(VectorType(s));
		return *this;
	}
	auto Fill(ValueType s) && -> MatrixImpl {

		data_.fill(VectorType(s));
		return std::move(*this);
	}
	
	[[nodiscard]] constexpr auto IsIdentity() -> Bool {

		if (!this->IsSquare())	return false;

		for (auto i = 0; i < COLUMN; ++i) {
			for (auto j = 0; j < ROW; ++j) {

				if (i == j) {
					if (At(i, j) != 1)	return false;
				} else {
					if (At(i, j) != 0)	return false;
				}
			}
		}
		return true;
	}
	[[nodiscard]] constexpr auto Transpose() const -> MatrixImpl<ValueType, COLUMN, ROW> {

		MatrixImpl<ValueType, COLUMN, ROW> buf{};
		for (auto i = 0; i < COLUMN; ++i) {
			for (auto j = 0; j < ROW; ++j) {

				buf(i, j) = At(j, i);
			}
		}
		return buf;
	}
	[[nodiscard]] constexpr auto Cut(Size cutRow, Size cutColumn) const -> MatrixImpl<ValueType, ROW - 1, COLUMN - 1> {

		MatrixImpl<ValueType, ROW - 1, COLUMN - 1> buf{};
		for (auto i = 0; i < ROW; ++i) {

			if (i == cutRow)	continue;
			for (auto j = 0; j < COLUMN; ++j) {

				if (j == cutColumn)	continue;

				auto inI = i > cutRow ? i - 1 : i;
				auto inJ = j > cutColumn ? j - 1 : j;
				buf(inI, inJ) = At(i, j);
			}
		}
		return buf;
	}
	[[nodiscard]] constexpr auto Norm() const -> FloatingValueType {

		auto buf = static_cast<FloatingValueType>(0);
		for (const auto& elem : dataAll_)	buf += Pow2(static_cast<FloatingValueType>(elem));

		return Sqrt(buf);
	}
	[[nodiscard]] constexpr auto NormInf() const -> ValueType {

		return std::max(dataAll_.begin(), dataAll_.end(), [](auto&& a, auto&& b) { return Abs(a) < Abs(b); });
	}
	template<Size _Pow>
	[[deprecated("not implemented")]] [[nodiscard]] constexpr auto Norm() const -> FloatingValueType {

		return std::numeric_limits<FloatingValueType>::quiet_NaN();
	}

	auto operator+=(ValueType rhv) -> MatrixImpl& {

		for (auto& datum : data_)	datum += rhv;
		return *this;
	}
	auto operator-=(ValueType rhv) -> MatrixImpl& {

		for (auto& datum : data_)	datum -= rhv;
		return *this;
	}
	auto operator*=(ValueType rhv) -> MatrixImpl& {

		for (auto& datum : data_)	datum *= rhv;
		return *this;
	}
	auto operator/=(ValueType rhv) -> MatrixImpl& {

		for (auto& datum : data_)	datum /= rhv;
		return *this;
	}
	auto operator+=(const MatrixImpl& rhv) -> MatrixImpl& {

		for (auto i = 0u; i < ROW; ++i)	data_[i] += rhv.GetRow(i);
		return *this;
	}
	auto operator-=(const MatrixImpl& rhv) -> MatrixImpl& {

		for (auto i = 0u; i < ROW; ++i)	data_[i] -= rhv.GetRow(i);
		return *this;
	}
	template<Scalar _TypeR, Size _RowR, Size _ColumnR>
	requires (_RowR == COLUMN && _ColumnR <= ROW)
	auto operator*=(const MatrixImpl<_TypeR, _RowR, _ColumnR>& rhv) -> MatrixImpl& {

		auto lhv = *this;
		Fill(0);
		for (auto i = 0; i < ROW; ++i) {
			for (auto j = 0; j < _ColumnR; ++j) {

				data_[i][j] += lhv.GetRow(i).Dot(rhv.GetColumn(j));
			}
		}
		return *this;
	}
};

}
