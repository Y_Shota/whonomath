#pragma once
#include <exception>

namespace Whono::Math {

/**
 * \brief 定義域外例外
 */
class OutOfDomain final : public std::exception {

public:
	explicit OutOfDomain(const char* message) noexcept
		: exception(message) {}
};

}
