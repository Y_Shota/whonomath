#pragma once
#include <utility>

#include "Implement.hpp"
#include "../BaseType.hpp"


namespace Whono::Math {

[[nodiscard]] constexpr auto DegreeOf(BaseFloatingPoint degree) -> DegreeImpl<BaseFloatingPoint> {

	return degree;
}
[[nodiscard]] constexpr auto RadianOf(BaseFloatingPoint radian) -> RadianImpl<BaseFloatingPoint> {

	return radian;
}
template<FloatingPoint _Type>
[[nodiscard]] constexpr auto DegreeTypeOf(_Type degree) -> DegreeImpl<_Type> {

	return degree;
}
template<FloatingPoint _Type>
[[nodiscard]] constexpr auto RadianTypeOf(_Type radian) -> RadianImpl<_Type> {

	return radian;
}

template<FloatingPoint _Type>
[[nodiscard]] constexpr auto Sin(const DegreeImpl<_Type>& angle) -> _Type {

	return Sin(angle.Radian());
}
template<FloatingPoint _Type>
[[nodiscard]] constexpr auto Cos(const DegreeImpl<_Type>& angle) -> _Type {

	return Cos(angle.Radian());
}
template<FloatingPoint _Type>
[[nodiscard]] constexpr auto SinCos(const DegreeImpl<_Type>& angle) -> std::pair<_Type, _Type> {
	
	return { Sin(angle), Cos(angle) };
}
template<FloatingPoint _Type>
[[nodiscard]] constexpr auto Tan(const DegreeImpl<_Type>& angle) -> _Type {

	return Tan(angle.Radian());
}
template<FloatingPoint _Type>
[[nodiscard]] constexpr auto Sin(const RadianImpl<_Type>& angle) -> _Type {

	return Sin(angle);
}
template<FloatingPoint _Type>
[[nodiscard]] constexpr auto Cos(const RadianImpl<_Type>& angle) -> _Type {

	return Cos(angle);
}
template<FloatingPoint _Type>
[[nodiscard]] constexpr auto SinCos(const RadianImpl<_Type>& angle) -> std::pair<_Type, _Type> {

	return { Sin(angle), Cos(angle) };
}
template<FloatingPoint _Type>
[[nodiscard]] constexpr auto Tan(const RadianImpl<_Type>& angle) -> _Type {

	return Tan(angle.Radian());
}

namespace Literals {

[[nodiscard]] constexpr auto operator""_deg(long double degree) -> DegreeImpl<BaseFloatingPoint> {

	return DegreeOf(static_cast<BaseFloatingPoint>(degree));
}
[[nodiscard]] constexpr auto operator""_rad(long double radian) -> RadianImpl<BaseFloatingPoint> {

	return RadianOf(static_cast<BaseFloatingPoint>(radian));
}
[[nodiscard]] constexpr auto operator""_degF(long double degree) -> DegreeImpl<Float> {

	return DegreeTypeOf(static_cast<Float>(degree));
}
[[nodiscard]] constexpr auto operator""_radF(long double radian) -> RadianImpl<Float> {

	return RadianTypeOf(static_cast<Float>(radian));
}
[[nodiscard]] constexpr auto operator""_degD(long double degree) -> DegreeImpl<Double> {

	return DegreeTypeOf(static_cast<Double>(degree));
}
[[nodiscard]] constexpr auto operator""_radD(long double radian) -> RadianImpl<Double> {

	return RadianTypeOf(static_cast<Double>(radian));
}
[[nodiscard]] constexpr auto operator""_deg(unsigned long long degree) -> DegreeImpl<BaseFloatingPoint> {

	return DegreeOf(static_cast<BaseFloatingPoint>(degree));
}
[[nodiscard]] constexpr auto operator""_rad(unsigned long long radian) -> RadianImpl<BaseFloatingPoint> {

	return RadianOf(static_cast<BaseFloatingPoint>(radian));
}
[[nodiscard]] constexpr auto operator""_degF(unsigned long long degree) -> DegreeImpl<Float> {

	return DegreeTypeOf(static_cast<Float>(degree));
}
[[nodiscard]] constexpr auto operator""_radF(unsigned long long radian) -> RadianImpl<Float> {

	return RadianTypeOf(static_cast<Float>(radian));
}
[[nodiscard]] constexpr auto operator""_degD(unsigned long long degree) -> DegreeImpl<Double> {

	return DegreeTypeOf(static_cast<Double>(degree));
}
[[nodiscard]] constexpr auto operator""_radD(unsigned long long radian) -> RadianImpl<Double> {

	return RadianTypeOf(static_cast<Double>(radian));
}
}

}
