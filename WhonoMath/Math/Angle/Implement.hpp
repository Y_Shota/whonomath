#pragma once
#include "../Scalar/Constant.hpp"
#include "../TypeTraits.hpp"

namespace Whono::Math {

template<FloatingPoint _T>
class RadianImpl;

template<FloatingPoint _T>
class DegreeImpl {

private:
	using Type = _T;

	Type angle_;
	
public:
	[[nodiscard]] constexpr DegreeImpl(Type angle = 0)
		: angle_(angle) {}

	[[nodiscard]] constexpr operator const Type&() const& { return angle_; }
	[[nodiscard]] operator Type&() & { return angle_; }
	[[nodiscard]] constexpr operator Type() const&& { return std::move(angle_); }
	[[nodiscard]] constexpr operator RadianImpl<_T>() const;

	[[nodiscard]] constexpr auto Radian() -> RadianImpl<Type>;
	
	template<FloatingPoint _TypeR>
	auto operator+=(const RadianImpl<_TypeR>& rhv) -> DegreeImpl&;
	template<FloatingPoint _TypeR>
	auto operator-=(const RadianImpl<_TypeR>& rhv) -> DegreeImpl&;
};

template<FloatingPoint _T>
class RadianImpl {

private:
	using Type = _T;

	Type angle_;

public:
	[[nodiscard]] constexpr RadianImpl(Type angle = 0)
		: angle_(angle) {}

	[[nodiscard]] constexpr operator const Type& () const& { return angle_; }
	[[nodiscard]] operator Type& () & { return angle_; }
	[[nodiscard]] constexpr operator Type& () const&& { return std::move(angle_); }
	[[nodiscard]] constexpr operator DegreeImpl<_T>() const;

	[[nodiscard]] constexpr auto Degree() -> DegreeImpl<Type>;
	
	template<FloatingPoint _TypeR>
	auto operator+=(const DegreeImpl<_TypeR>& rhv) -> RadianImpl&;
	template<FloatingPoint _TypeR>
	auto operator-=(const DegreeImpl<_TypeR>& rhv) -> RadianImpl&;
};

template<FloatingPoint _T>
constexpr DegreeImpl<_T>::operator RadianImpl<_T>() const {

	return angle_ * PI_T<_T> / 180;
}
template<FloatingPoint _T>
constexpr auto DegreeImpl<_T>::Radian() -> RadianImpl<_T> {

	return angle_ * PI_T<_T> / 180;
}

template <FloatingPoint _T>
constexpr RadianImpl<_T>::operator DegreeImpl<_T>() const {

	return angle_ * 180 / PI_T<_T>;
}
template<FloatingPoint _T>
constexpr auto RadianImpl<_T>::Degree() -> DegreeImpl<_T> {

	return angle_ * 180 / PI_T<_T>;
}


template<FloatingPoint _T>
template<FloatingPoint _TypeR>
auto DegreeImpl<_T>::operator+=(const RadianImpl<_TypeR>& rhv) -> DegreeImpl& {

	angle_ += rhv.Degree();
	return *this;
}
template<FloatingPoint _T>
template<FloatingPoint _TypeR>
auto DegreeImpl<_T>::operator-=(const RadianImpl<_TypeR>& rhv) -> DegreeImpl& {

	angle_ -= rhv.Degree();
	return *this;
}

template<FloatingPoint _T>
template<FloatingPoint _TypeR>
auto RadianImpl<_T>::operator+=(const DegreeImpl<_TypeR>& rhv) -> RadianImpl& {

	angle_ += rhv.Radian();
	return *this;
}
template<FloatingPoint _T>
template<FloatingPoint _TypeR>
auto RadianImpl<_T>::operator-=(const DegreeImpl<_TypeR>& rhv) -> RadianImpl& {

	angle_ -= rhv.Radian();
	return *this;
}

}
