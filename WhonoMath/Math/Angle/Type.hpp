#pragma once
#include "../BaseType.hpp"
#include "../TypeTraits.hpp"

namespace Whono::Math {

template<FloatingPoint _T>
class DegreeImpl;

template<FloatingPoint _T>
using DegreeT = DegreeImpl<_T>;
using DegreeF = DegreeT<Float>;
using DegreeD = DegreeT<Double>;
using Degree = DegreeT<BaseFloatingPoint>;


template<FloatingPoint _T>
class RadianImpl;

template<FloatingPoint _T>
using RadianT = RadianImpl<_T>;
using RadianF = RadianT<Float>;
using RadianD = RadianT<Double>;
using Radian = RadianT<BaseFloatingPoint>;


}
