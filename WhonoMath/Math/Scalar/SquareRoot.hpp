#pragma once
#include "Detail.hpp"
#include "../TypeTraits.hpp"

namespace Whono::Math {

template<Scalar _T, class _FloatType = ToFloatingPointType<_T>>
[[nodiscard]] constexpr auto Sqrt(_T s) -> _FloatType {

	using FloatType = _FloatType;
	
	auto x = s / FloatType{ 2 };
	auto prev = FloatType{ 0 };
	
	for (;;) {

		prev = x;
		x = (s / x + x) / 2;

		if (Feq(x, prev))	return x;
	}
}

}
