#pragma once
#include "../TypeTraits.hpp"
#include "Absolute.hpp"

namespace Whono::Math {

template<Scalar _T, Scalar _U>
[[nodiscard]] constexpr auto CopySign(_T x, _U sign) -> _T {

	return Abs(x) * (sign >= 0 ? 1 : -1);
}

}
