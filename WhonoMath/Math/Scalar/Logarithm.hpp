#pragma once
#include <limits>

#include "../TypeTraits.hpp"
#include "../Exception.hpp"

namespace Whono::Math {

namespace Detail {

template<FloatingPoint _T>
constexpr auto LOG_HALF = static_cast<_T>(-0.69314718055994530941723212145818);

}

template<Scalar _T, class _FloatType = ToFloatingPointType<_T>>
[[nodiscard]] constexpr auto Log(_T x) -> _FloatType {

	using FloatType = _FloatType;

	if (x <= 0)		throw OutOfDomain("not defined of negative number");
	
	if (x == 0)		return -std::numeric_limits<FloatType>::infinity();
	if (x == 1)		return +FloatType{ 0 };

	if (x == std::numeric_limits<FloatType>::infinity())
					return std::numeric_limits<FloatType>::infinity();

	if (x == 0.5)	return Detail::LOG_HALF<FloatType>;
	
	if (x >= 2)		return -Log(FloatType{ 1 } / x);

	if (x < 0.5)	return Detail::LOG_HALF<FloatType> + Log(x / FloatType{ 0.5 });

	auto sum = FloatType{ 0 };
	auto pow = FloatType{ 1 };
	for (auto i = 1;; ++i) {

		pow *= x - 1;
		auto t = (i % 2 ? 1 : -1) * pow / i;
		if (Feq(sum, sum + t))	return sum;
		sum += t;
	}
}

}
