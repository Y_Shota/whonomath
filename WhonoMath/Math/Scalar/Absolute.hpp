#pragma once
#include "../TypeTraits.hpp"

namespace Whono::Math {

template<Scalar _T>
[[nodiscard]] constexpr auto Abs(_T x) -> _T {

	return x >= 0 ? x : -x;
}

}