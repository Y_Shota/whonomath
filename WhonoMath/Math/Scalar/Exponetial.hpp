#pragma once
#include <cmath>
#include <limits>

#include "../TypeTraits.hpp"
#include "Detail.hpp"
#include "Factorial.hpp"

namespace Whono::Math {

namespace Detail {

template<FloatingPoint _T>
constexpr auto E_P2 = static_cast<_T>(7.389056098930650227230427460575);
template<FloatingPoint _T>
constexpr auto E_N2 = static_cast<_T>(0.13533528323661269189399949497248);

}

template<Scalar _T, class _FloatType = ToFloatingPointType<_T>>
[[nodiscard]] constexpr auto Exp(_T x) -> _FloatType {

	using FloatType = _FloatType;
	
	if (x == 0)	return 1;
	
	if (x == std::numeric_limits<FloatType>::infinity())	return std::numeric_limits<FloatType>::infinity();
	if (x == -std::numeric_limits<FloatType>::infinity())	return +FloatType{ 0 };

	if (x == 2.)	return Detail::E_P2<FloatType>;
	if (x == -2.)	return Detail::E_N2<FloatType>;
	
	if (x >= 5.)	return Detail::E_P2<FloatType> * Exp(x - 2);
	if (x <= -5.)	return Detail::E_N2<FloatType> * Exp(x + 2);
	
	auto sum = FloatType{ 0 };
	auto pow = FloatType{ 1 };
	for (auto i = 0;; ++i) {

		pow *= x;
		auto t = pow / static_cast<FloatType>(Fact(i));
		if (Feq(sum, sum + t))	return sum;
		sum += t;
	}
}

}
