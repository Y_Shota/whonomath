#pragma once
#include <limits>
#include <utility>

#include "Constant.hpp"
#include "../TypeTraits.hpp"

namespace Whono::Math {

template<Scalar _T>
[[nodiscard]] constexpr auto Abs(_T x) -> _T {

	return x >= 0 ? x : -x;
}
template<Scalar _T, class _RetType = ToFloatingPointType<_T>>
[[nodiscard]] constexpr auto Sqrt(_T s) -> _RetType {

	auto x = s / static_cast<_RetType>(2);
	auto prev = static_cast<_RetType>(0);

	while (x != prev) {

		prev = x;
		x = (x + s / x) / static_cast<_RetType>(2);
	}
	return x;
}
template<Scalar _T>
[[nodiscard]] constexpr auto Pow(_T x, ULong y) -> _T {

	return y == 0 ? 1 : x * Pow(x, y - 1);
}
template<Scalar _T>
[[nodiscard]] constexpr auto Pow2(_T x) -> _T {

	return Pow(x, 2);
}
template<Integral _T>
[[nodiscard]] constexpr auto Fact(_T x) -> _T {

	return x == 0 ? 1 : x * Fact(x - 1);
}
template<Scalar _T, Scalar _U>
[[nodiscard]] constexpr auto CopySign(_T x, _U sign) -> _T {

	return Abs(x) * (sign >= 0 ? 1 : -1);
}
template<FloatingPoint _T>
[[nodiscard]] constexpr auto Feq(_T x, _T y) -> Bool {

	return Abs(x - y) <= static_cast<_T>(1e-3f);
}
template<Scalar _T, class _RetType = ToFloatingPointType<_T>>
[[nodiscard]] constexpr auto Sin(_T x) -> _RetType {

	auto sum = static_cast<_RetType>(0);
	for (auto i = 0;; ++i) {

		auto t = Pow<_RetType>(-1, i) / Fact(2 * i + 1) * Pow<_RetType>(x, 2 * i + 1);
		if (Feq(sum, sum + t))	return sum;
		sum += t;
	}
}
template<Scalar _T, class _RetType = ToFloatingPointType<_T>>
[[nodiscard]] constexpr auto Cos(_T x) -> _RetType {

	auto sum = static_cast<_RetType>(0);
	for (auto i = 0;; ++i) {

		auto t = Pow<_RetType>(-1, i) / Fact(2 * i) * Pow<_RetType>(x, 2 * i);
		if (Feq(sum, sum + t))	return sum;
		sum += t;
	}
}
template<Scalar _T, class _RetType = ToFloatingPointType<_T>>
[[nodiscard]] constexpr auto SinCos(_T x) -> std::pair<_RetType, _RetType> {

	return { Sin(x), Cos(x) };
}
template<Scalar _T, class _RetType = ToFloatingPointType<_T>>
[[nodiscard]] constexpr auto Tan(_T x) -> _RetType {

	return Sin(x) / Cos(x);
}
template<Scalar _T, class _RetType = ToFloatingPointType<_T>>
[[nodiscard]] constexpr auto Asin(_T x) -> _RetType {

	auto absX = Abs(x);

	if (absX > 1)	return 0;
	if (x == 0)		return 0;
	if (absX == 1)	return PI_DIV2_T<_RetType> * x;

	if (absX > 1 / Sqrt<_RetType>(2) && absX < 1) {

		return CopySign(PI_DIV2_T<_RetType> - Asin(Sqrt<_RetType>(1 - x * x)), x);
	}

	auto sum = static_cast<_RetType>(0);
	for (auto i = 0;; ++i) {

		auto t = Fact(2 * i) / (Pow(4, i) * Pow(static_cast<_RetType>(Fact(i)), 2) * (2 * i + 1)) * Pow<_RetType>(x, 2 * i + 1);
		if (Feq(sum, sum + t))	return sum;
		sum += t;
	}
}
template<Scalar _T, class _RetType = ToFloatingPointType<_T>>
[[nodiscard]] constexpr auto Acos(_T x) -> _RetType {

	auto absX = Abs(x);

	if (absX > 1.f)	return 0;
	if (x == 0.f)	return PI_DIV2_T<_RetType>;
	if (x == 1.f)	return 0;
	if (x == -1.f)	return PI_T<_RetType>;

	auto sum = static_cast<_RetType>(0);
	for (auto i = 0;; ++i) {

		auto t = Fact(2 * i) / (Pow(4, i) * Pow(static_cast<_RetType>(Fact(i)), 2) * (2 * i + 1)) * Pow<_RetType>(x, 2 * i + 1);
		if (Feq(sum, sum + t))	return PI_DIV2_T<_RetType> -sum;
		sum += t;
	}
}
template<Scalar _T, class _RetType = ToFloatingPointType<_T>>
[[nodiscard]] constexpr auto Atan(_T x) -> _RetType {

	if (Abs(x) == 1)									return PI_DIV4_T<_RetType> * x;
	if (x == 0)											return 0;
	if (x == std::numeric_limits<_RetType>::infinity())	return PI_DIV2_T<_RetType>;

	auto absX = Abs(x);

	if (absX > 1) {
		constexpr auto sqrt2 = Sqrt<_RetType>(2);
		if (absX > sqrt2 + 1) {

			return PI_DIV2_T<_RetType> - Atan(1 / static_cast<_RetType>(x));
		}
		if (absX > sqrt2 - 1 && absX <= sqrt2 + 1) {

			return PI_DIV4_T<_RetType> + Atan((x - 1) / static_cast<_RetType>(x + 1));
		}
	}

	auto sum = static_cast<_RetType>(0);
	for (auto i = 0;; ++i) {

		auto t = Pow<_RetType>(-1, i) / (2 * i + 1) * Pow<_RetType>(x, 2 * i + 1);
		if (Feq(sum, sum + t))	return sum;
		sum += t;
	}
}
template<Scalar _T, class _RetType = ToFloatingPointType<_T>>
[[nodiscard]] constexpr auto Atan2(_T y, _T x) -> _RetType {

	if (x == 0)	return CopySign(PI_DIV2_T<_RetType>, y);
	auto theta = static_cast<_RetType>(y) / x;
	if (x > 0)	return Atan(theta);
	return Atan(theta) + CopySign(PI_T<_RetType>, y);
}

}
