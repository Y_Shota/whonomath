#pragma once
#include <utility>

#include "../TypeTraits.hpp"
#include "Detail.hpp"
#include "Factorial.hpp"
#include "PowerN.hpp"
#include "Reciprocal.hpp"

namespace Whono::Math {

template<Scalar _T, class _FloatType = ToFloatingPointType<_T>>
[[nodiscard]] constexpr auto Sin(_T x) -> _FloatType {

	using FloatType = _FloatType;
	
	auto sum = FloatType{ 0 };
	for (auto i = 0;; ++i) {

		auto n = i * 2 + 1;
		auto t = (i % 2 ? -1 : 1) * PowN(x, n) / static_cast<FloatType>(Fact(n));
		if (Feq(sum, sum + t))	return sum;
		sum += t;
	}
}

template<Scalar _T, class _FloatType = ToFloatingPointType<_T>>
[[nodiscard]] constexpr auto Cos(_T x) -> _FloatType {

	using FloatType = _FloatType;

	auto sum = FloatType{ 0 };
	for (auto i = 0;; ++i) {

		auto n = i * 2;
		auto t = (i % 2 ? -1 : 1) * PowN(x, n) / static_cast<FloatType>(Fact(n));
		if (Feq(sum, sum + t))	return sum;
		sum += t;
	}
}

template<Scalar _T, class _FloatType = ToFloatingPointType<_T>>
[[nodiscard]] constexpr auto SinCos(_T x) -> std::pair<_FloatType, _FloatType> {

	return { Sin(x), Cos(x) };
}

template<Scalar _T, class _FloatType = ToFloatingPointType<_T>>
[[nodiscard]] constexpr auto Tan(_T x) -> _FloatType {

	return Sin(x) / Cos(x);
}

template<Scalar _T, class _FloatType = ToFloatingPointType<_T>>
[[nodiscard]] constexpr auto Sec(_T x) -> _FloatType {

	return Reciprocal(Cos(x));
}

template<Scalar _T, class _FloatType = ToFloatingPointType<_T>>
[[nodiscard]] constexpr auto Csc(_T x) -> _FloatType {

	return Reciprocal(Sin(x));
}

template<Scalar _T, class _FloatType = ToFloatingPointType<_T>>
[[nodiscard]] constexpr auto Cot(_T x) -> _FloatType {

	return Cos(x) / Sin(x);
}

}
