#pragma once
#include "../TypeTraits.hpp"

namespace Whono::Math {

template<Scalar _T, class _FloatType = ToFloatingPointType<_T>>
[[nodiscard]] constexpr auto Reciprocal(_T x) -> _FloatType {

	using FloatType = _FloatType;

	return FloatType{ 1 } / x;
}

}
