#pragma once
#include "../TypeTraits.hpp"


namespace Whono::Math {

template<Scalar _T>
[[nodiscard]] constexpr auto Pow2(_T x) -> _T {

	return x * x;
}
template<Scalar _T>
[[nodiscard]] constexpr auto Pow3(_T x) -> _T {

	return x * x * x;
}
template<Scalar _T>
[[nodiscard]] constexpr auto Pow4(_T x) -> _T {

	return x * x * x * x;
}
template<Scalar _T>
[[nodiscard]] constexpr auto PowN(_T x, ULong n) -> _T {

	return n == 0 ? 1
		: n == 1 ? x
		: n % 2 ? x * Pow2(PowN(x, n / 2))
		: Pow2(PowN(x, n / 2));
}

}
