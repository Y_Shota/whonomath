#pragma once
#include "../TypeTraits.hpp"

namespace Whono::Math {

template <Scalar _T>
constexpr auto PI_T = static_cast<_T>(3.1415926535897932384626433832795);
constexpr auto PI = PI_T<BaseFloatingPoint>;

template <Scalar _T>
constexpr auto PI_DIV2_T = PI_T<_T> / 2;
constexpr auto PI_DIV2 = PI_DIV2_T<BaseFloatingPoint>;

template <Scalar _T>
constexpr auto PI_DIV4_T = PI_T<_T> / 4;
constexpr auto PI_DIV4 = PI_DIV4_T<BaseFloatingPoint>;

template<Scalar _T>
constexpr auto E_T = static_cast<_T>(2.7182818284590452353602874713527);
constexpr auto E = E_T<BaseFloatingPoint>;

}
