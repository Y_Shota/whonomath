#pragma once
#include <limits>

#include "../TypeTraits.hpp"
#include "Absolute.hpp"

namespace Whono::Math {

template<FloatingPoint _T, FloatingPoint _U>
[[nodiscard]] constexpr auto Feq(_T x, _U y) -> Bool {

	return Abs(x - y) <= std::numeric_limits<GetPrecisionType<_T, _U>>::epsilon();
}

}
