#pragma once
#include <cstdint>
#include <cstddef>

namespace Whono {


using Byte = std::int8_t;
using Short = std::int16_t;
using Int = std::int32_t;
using Long = std::int64_t;

using UByte = std::uint8_t;
using UShort = std::uint16_t;
using UInt = std::uint32_t;
using ULong = std::uint64_t;

using Size = std::size_t;

using Float = float;
using Double = double;

using Bool = bool;

using BaseIntegral = Int;
using BaseFloatingPoint = Float;
}
